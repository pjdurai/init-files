;;; General

(setq inhibit-startup-message t)
(tool-bar-mode -1)
(toggle-scroll-bar -1)

(setq magit-last-seen-setup-instructions "1.4.0")
(setq ring-bell-function 'ignore)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

;; Performance tweaks.
;; Disable this because golang mode gets really slow with this.
(global-eldoc-mode 0)
;;;; Don't show the function names on the mode line. Makes things slower.
;;(which-function-mode 0)
;;(setq projectile-mode-line "foo")

;; To see all the fonts run M-x ielm and (font-family-list)

(set-face-font 'default "-*-Menlo-normal-normal-normal-*-18-*-*-*-m-0-iso10646-1")

;;;; Adjust font size dynamically depending on resolution.
(defun fontify-frame (&optional frame)
  (interactive)
  (let ((target (or frame (window-frame))))
    (if window-system
        (if (or
             (> (frame-pixel-height) 2000)
             (> (frame-pixel-width) 2000))
            (set-frame-parameter target 'font "Menlo 19")
          (set-frame-parameter target 'font "Menlo 14")))))


(add-to-list 'load-path "~/site-lisp/")

;;;; Prelude/Projectile settings

(setq prelude-guru nil)
(setq whitespace-style '())

(setq projectile-indexing-method 'alien)

;; Improves performance
;; https://github.com/bbatsov/projectile/issues/657
(projectile-global-mode)
(setq projectile-switch-project-action 'projectile-dired)

;;; Identation
;;(setq js-indent-level 2)
;;(setq ruby-indent-level 2)

;; Tramp
(set-default 'tramp-default-method "ssh")

(require 'tramp)
;;/ssh:/i844441@mo-1e2ce883b.mo.sap.corp:/home/i844441/mlinks
;;/i844441@mo-1e2ce883b.mo.sap.corp:/home/i844441/mlinks


;;;; Emacs Server
;; After a system reboot, without this folder emacs server start fails.
(unless (file-exists-p "/var/folders/p8/81l2kxmx3wq9y6mkf8521qx80000gn/T/emacs501")
  (make-directory "/var/folders/p8/81l2kxmx3wq9y6mkf8521qx80000gn/T/emacs501"))

(require 'server)
(and (>= emacs-major-version 23)
     (defun server-ensure-safe-dir (dir) "Noop" t))
(server-start)


;;;; Package Management

(defun install-package-if-not-installed (pkg)
  (when (not (package-installed-p pkg))
    (package-refresh-contents)
    (package-install pkg)))

(install-package-if-not-installed 'wgrep)
(install-package-if-not-installed 'helm-swoop)
(install-package-if-not-installed 'nov)  ;; Epub Editing.
(install-package-if-not-installed 'org-bullets)
(package-install 'use-package)
(require 'use-package)

;;; Helm

(require 'helm-swoop)
(require 'prelude-helm-everywhere)

(global-set-key (kbd "M-i") 'helm-swoop)
(global-set-key (kbd "M-I") 'helm-swoop-back-to-last-point)
(global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)

(install-package-if-not-installed 'markdown-mode)

;;;; Visible Bookmarks.
(install-package-if-not-installed 'bm)
(require 'bm)

(global-set-key (kbd "<C-f2>") 'bm-toggle)
(global-set-key (kbd "<f2>")   'bm-next)
(global-set-key (kbd "<S-f2>") 'bm-previous)

(global-set-key (kbd "<f7>")   'compile)
(global-set-key (kbd "<f8>")   'other-frame)


;;;; Org mode

(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)

(setq org-tags-match-list-sublevels 'indented)

(global-set-key (kbd "<f9> o") 'org-clock-out)
(global-set-key (kbd "<f9> i") 'org-clock-in)
(global-set-key (kbd "<f9> r") 'org-clock-in-last) ;Resume last task
(global-set-key (kbd "<f9> q") 'org-clock-cancel)  ; Cancel the current clock. Started by mistake? Quick context switch?
(global-set-key (kbd "<f9> a") 'org-archive-subtree-default) ; Archive entry


(setq org-todo-keywords
      '((sequence "TODO(t)" "DOING(o)" "BLOCKED(b)" "REVIEW(r)"
                  "VERIFY(v)" "|" "DONE(d)" "CANCELLED(c)" "SOMEDAY(s)")))

(setq org-todo-keyword-faces
      '(("TODO" . org-warning)
        ("DOING" . "yellow")
        ("BLOCKED" . "red")
        ("REVIEW" . "orange")
        ("DONE" . "green")
        ("SOMEDAY" . "blue")
        ("ARCHIVED" .  "blue")))

(setq org-agenda-files (list  "~/org-files/work.org" "~/org-files/wiki" "~/org-files/habits.org" ))

(defun org-journal-find-location ()
  ;; Open today's journal, but specify a non-nil prefix argument in order to
  ;; inhibit inserting the heading; org-capture will insert the heading.
  (org-journal-new-entry t)
  ;; Position point on the journal's top-level heading so that org-capture
  ;; will add the new entry as a child entry.
  (goto-char (point-min)))


(setq org-capture-templates
      '(("t" "Todo" entry (file+headline "~/org-files/work.org" "Tasks")
         "* TODO %?\n  %i\n  ")
        ("j" "Journal entry" entry (function org-journal-find-location)
         "* %(format-time-string org-journal-time-format)%^{Title}\n%i%?")))


(use-package org-journal
             :ensure t
             :defer t
             :custom
             (org-journal-dir "~/org-files/journal/")
             (org-journal-date-format "%A, %d %B %Y")
             (org-journal-file-type 'monthly))

(require 'org-journal)

(install-package-if-not-installed 'org-sidebar)


(require 'ob-perl6) ;; For OrgBable. https://github.com/LLFourn/p6-and-chill

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
;;(install-package-if-not-installed 'org-pomodoro)
;;(require 'org-pomodoro)


(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (python . t)
   (shell . t)
   (ruby . t)
   (perl . t)
   (perl6 . t)
   ;;(haskell . t)
   (js . t)
   (gnuplot . t)
   (C . t)
   ))

(require 'org-bullets)
(add-hook 'org-mode-hook 'org-bullets-mode)

;;;; org-super-agenda

;;(install-package-if-not-installed 'org-super-agenda)
;;; Haskell

;(package-install 'intero)
;(add-hook 'haskell-mode-hook 'intero-mode)

;; OrgWiki
(add-to-list 'load-path "~/site-lisp/org-wiki")
(require 'org-wiki)

(setq org-wiki-location "~/org-files/wiki")

;; Need the following for orgwiki-search to work
(defadvice rgrep (around rgrep-init)
  "Init grep defaults before calling rgrep non-interactively."
  (when (not (called-interactively-p))
    (grep-compute-defaults))
  ad-do-it)

(ad-activate 'rgrep)


;;; Gnu PGP

(require 'epa-file)
(epa-file-enable)
(setq epa-pinentry-mode 'loopback)


;; Common Lisp

(load (expand-file-name "~/.roswell/helper.el"))
;(setq inferior-lisp-program "ros -Q run")

(prefer-coding-system 'utf-8-unix)

(with-eval-after-load 'slime
  ;;;(slime-setup '(slime-fancy slime-banner slime-repl-ansi-color))
  (slime-setup '(slime-fancy slime-asdf slime-tramp))

  ;; indentation tweaks
  (put 'define-package 'common-lisp-indent-function '(4 2))

  (setf slime-lisp-implementations
        `((roswell ("ros" "-Q" "run"))))
  (setf slime-default-lisp 'roswell))

(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'lisp-mode-hook 'turn-on-auto-fill)

;; (install-package-if-not-installed 'yasnippet)
;; (require 'yasnippet)
;; (yas-global-mode 1)


;; Prelude
;; Fly check disable
(with-eval-after-load 'prelude-custom
  (setq prelude-whitespace nil)
  (setq prelude-flyspell nil))

(setq prelude-flyspell nil)
(remove-hook 'prog-mode 'flycheck-mode)
;;(setq flycheck-highlighting-mode 'lines)
(global-flycheck-mode -1)

;; Perspective
;;(persp-mode)
;(require 'persp-projectile)


;; My Functions
(defun copy-file-name()
  (interactive)
  (kill-new buffer-file-name))

(add-hook 'groovy-mode-hook
          (lambda ()
            (c-set-offset 'label 2)))

;;; "Maximize buffer"
(defun toggle-maximize-buffer ()
  (interactive)
  (if (= 1 (length (window-list)))
      (jump-to-register '_)
    (progn
      (window-configuration-to-register '_)
      (delete-other-windows))))

;;;;;
;;;; Org Mode Bable helper functions.
;;;; http://kitchingroup.cheme.cmu.edu/blog/2015/03/19/Restarting-org-babel-sessions-in-org-mode-more-effectively/

(defun src-block-in-session-p (&optional name)
  "Return if src-block is in a session of NAME.
NAME may be nil for unnamed sessions."
  (let* ((info (org-babel-get-src-block-info))
         (lang (nth 0 info))
         (body (nth 1 info))
         (params (nth 2 info))
         (session (cdr (assoc :session params))))

    (cond
     ;; unnamed session, both name and session are nil
     ((and (null session)
	   (null name))
      t)
     ;; Matching name and session
     ((and
       (stringp name)
       (stringp session)
       (string= name session))
      t)
     ;; no match
     (t nil))))

(defun org-babel-restart-session-to-point (&optional arg)
  "Restart session up to the src-block in the current point.
Goes to beginning of buffer and executes each code block with
`org-babel-execute-src-block' that has the same language and
session as the current block. ARG has same meaning as in
`org-babel-execute-src-block'."
  (interactive "P")
  (unless (org-in-src-block-p)
    (error "You must be in a src-block to run this command"))
  (let* ((current-point (point-marker))
	 (info (org-babel-get-src-block-info))
         (lang (nth 0 info))
         (params (nth 2 info))
         (session (cdr (assoc :session params))))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward org-babel-src-block-regexp nil t)
	;; goto start of block
        (goto-char (match-beginning 0))
	(let* ((this-info (org-babel-get-src-block-info))
	       (this-lang (nth 0 this-info))
	       (this-params (nth 2 this-info))
	       (this-session (cdr (assoc :session this-params))))
	    (when
		(and
		 (< (point) (marker-position current-point))
		 (string= lang this-lang)
		 (src-block-in-session-p session))
	      (org-babel-execute-src-block arg)))
	;; move forward so we can find the next block
	(forward-line)))))

(defun org-babel-kill-session ()
  "Kill session for current code block."
  (interactive)
  (unless (org-in-src-block-p)
    (error "You must be in a src-block to run this command"))
  (save-window-excursion
    (org-babel-switch-to-session)
    (kill-buffer)))

(defun org-babel-remove-result-buffer ()
  "Remove results from every code block in buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward org-babel-src-block-regexp nil t)
      (org-babel-remove-result))))

;;;; Eye Browse
;(eyebrowse-mode t)

;;; Atomic chrome
(install-package-if-not-installed 'atomic-chrome)
(require 'atomic-chrome)
(atomic-chrome-start-server)

;; perl6
(install-package-if-not-installed 'perl6-mode)

;;; frame-workflow
;; (add-to-list 'load-path "~/site-lisp/frame-workflow/")
;; (require 'frame-workflow)
;; (frame-workflow-mode 1)
;; (autoload 'helm-frame-workflow "helm-frame-workflow")
;; (setq projectile-switch-project-action #'frame-workflow-switch-directory-frame)


;;;; Persp-mode

;; (with-eval-after-load "persp-mode-autoloads"
;;   (setq wg-morph-on nil) ;; switch off animation
;;   (setq persp-autokill-buffer-on-remove 'kill-weak)
;;   (add-hook 'after-init-hook #'(lambda () (persp-mode 1))))

;; (with-eval-after-load "persp-mode-projectile-bridge-autoloads"
;;   (add-hook 'persp-mode-projectile-bridge-mode-hook
;;             #'(lambda ()
;;                 (if persp-mode-projectile-bridge-mode
;;                     (persp-mode-projectile-bridge-find-perspectives-for-all-buffers)
;;                   (persp-mode-projectile-bridge-kill-perspectives))))
;;   (add-hook 'after-init-hook
;;             #'(lambda ()
;;                 (persp-mode-projectile-bridge-mode 1))
;;             t))


;;;; Golang

;(setenv "PATH" (concat (getenv "PATH") ":/Users/i844441/goprojects/bin/"))
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize)
  (exec-path-from-shell-copy-env "GOPATH"))

(add-hook 'go-mode-hook
          (lambda ()
            (setq-default)
            (setq tab-width 4)
            (setq standard-indent 4)
            (setq indent-tabs-mode nil)
            (eldoc-mode 0)))

(install-package-if-not-installed 'go-complete)
(add-hook 'completion-at-point-functions 'go-complete-at-point)

(setq gofmt-command "goimports")

;;;;; Julia
;; (require 'julia-mode)
;; (require 'julia-repl)
;; (add-hook 'julia-mode-hook 'julia-repl-mode)

;;;;; direnv

(install-package-if-not-installed 'direnv)
(use-package direnv
  :config
  (direnv-mode))


(defun package-reinstall-all-activated-packages ()
  "Refresh and reinstall all activated packages."
  (interactive)
  (package-refresh-contents)
  (dolist (package-name package-activated-list)
    (when (package-installed-p package-name)
      (unless (ignore-errors                   ;some packages may fail to install
                (package-reinstall package-name))
        (warn "Package %s failed to reinstall" package-name)))))


;;; Kubernetes
(use-package kubernetes
  :ensure t
  :commands (kubernetes-overview))

;; No automatic refresh. 'g' for refresh.
(setq kubernetes-poll-frequency 3600)
(setq kubernetes-redraw-frequency 3600)

(setenv "KUBECONFIG" "/Users/i844441/ops/kubernetes/canary/kubeconfig.yaml")
(setenv "KUBECONFIG" "/Users/i844441/ops/kubernetes/canary/kubeconfig.yaml")
(setenv "KUBECONFIG" "/Users/i844441/ops/kubernetes/i844441dv2/kubeconfig.yaml")


;;; Disable which-key  (pops up the huge helper window when you are halfway into a key combo)
(which-key-mode 0)
